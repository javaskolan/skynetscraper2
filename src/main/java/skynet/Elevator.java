package skynet;

import java.util.ArrayList;
import java.util.List;

/**
 * Handles the elevatorVolume
 *
 * @author Swartt
 */
public class Elevator {

    private List<Person> persons = new ArrayList<>();


    private int pos = 0;


    public Elevator() {

    }

    /**
     * Lets the user select a floor to send the elevatorVolume to a floor of
     * their choice
     */
    public void sendElevator(int toFloor) { // was userInput ??

        pos = toFloor;
    }

    /**
     * Handles the entering into and the leaving off the elevatorVolume
     */
    // CONSIDER in the future, to return boolean true/false
    public boolean enter(Person person) { 
        if (hasPlace()) {
            persons.add(person);
            return true;
        } else {
            System.out.println("Elevator is full " + person.getName() + " can't enter");
            return false;
        }
    }

    public void exit(Person person) {
        persons.remove(person);

    }

    

    public int position() {
        return pos;
    }

    public boolean hasPerson(Person person) {
        return persons.contains(person);

    }

    public boolean empty() {
        return persons.isEmpty();

    }

    public boolean hasPlace() {
    
        boolean place = false;
        if (persons.size() <= 6) {
            place = true;
        }

        return place;
    }
    
    
        public void printPeople() {

            for (Person p : persons) {
                System.out.println(p.getName() + " Wants to  floor #  " + p.getWantedFloor());
            }

        System.out.print("\n\n");
    }
        
            
    public Person findPerson(String name) {
        for (Person p : persons) {
            if (p.getName().equals(name))
                return p;
        }
        
        return null;
    }
    
}
