/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package skynet;

import com.sun.org.apache.bcel.internal.generic.GETFIELD;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author Swartt
 */
public class Timer {

   public boolean loseState = false;

   public Timer(int minutes) {
      ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor(poolsAdminJob -> {
         Thread t = new Thread(poolsAdminJob);
         t.setDaemon(true);
         return t;
      });
      executor.schedule(() -> {
         loseState = true;
      }, minutes, TimeUnit.MINUTES);

      executor.shutdown();
   }
}
