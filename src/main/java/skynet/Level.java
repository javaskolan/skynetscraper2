/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package skynet;

/**
 * Game configuration depends on selected difficulaty. Part of this is:
 *
 * <ol>
 * <li>Number of persons to dispatch to floors
 * <li>Number of floors to create
 * <li>ammount of minutes game is active
 * </ol>
 *Getter methods:
 * <ol>
 * <li>getPerson returns persons
 * <li>getFloors returns floors
 * <li>getMinutes returns minutes 
 </ol>
 * @author c0deCtupus
 */
public enum Level {

    EASY(5, 10, 5), MEDIUM(10, 10, 3), HARD(15, 20, 5);

    private final int persons;
    private final int floors;
    private final int minutes;

    private Level(int persons, int floors, int time) {
        this.persons = persons;
        this.floors = floors;
        this.minutes = time;
    }

    public int getPersons() {
        return persons;
    }

    public int getFloors() {
        return floors;
    }

    public int getMinutes() {
        return minutes;
    }

}
