package skynet;

import java.io.Console;
import java.util.List;

/**
 *
 *
 * @author c0deCtupus
 */
public class GameEngine {

   private static final Console c = System.console();
   
   /**
    * Asks the User for input and creates a game based on the difficulty
    * selected<p>
    *
    * @param ignored
    */
   public GameEngine() {
   }
//   public void setLevel(Level level){
//      
//      level = getLevelChoice();
//      Floor[] building = new Floor[level.getFloors()];
//      Timer timer = new Timer(level.getMinutes());
//
//      //Create instances of Floor and add the to the building array
//      for (int i = 0; i < building.length; i++) {
//         building[i] = new Floor();
//      }
//
//      // Adds new persons based on difficulty on floor 0
//      Persons.populateFloor(building[0], level.getPersons());
//
//      // Create an elevator
//      Elevator elevator = new Elevator();
//      
//   }
   public void start(){
      
      setLevel(getLevelChoice());
      Level level = getLevelChoice();
      Floor[] building = new Floor[level.getFloors()];

      Timer timer = new Timer(level.getMinutes());

      //Create instances of Floor and add the to the building array
      for (int i = 0; i < building.length; i++) {
         building[i] = new Floor();
      }

      // Adds new persons based on difficulty on floor 0
      Persons.populateFloor(building[0], level.getPersons());

      // Create an elevator
      Elevator elevator = new Elevator();
   
      //checks if your time is up or not
      while (timer.loseState == false) {
         // Print options
//         public userChoice(){
//         String UserInput = c.readLine("Input:\n"
//                                       + "enter -> get a person in elevator\n"
//                                       + "exit -> get people out from "
//                                       + "elevator\nsend -> send elevator to floor\nshow -> show population "
//                                       + "on floors\nelev -> show people in elevator\ncheck -> check if win\nend -> "
//                                       + "To exit game\nEnter choice: ").toLowerCase();
//         }
         // Main game logic, asks for user input and runs the logic he requiered
         // to preform the asked action
         switch (User.Choice()) {
            case "enter":
               while (elevator.hasPlace()) {
                  User.enter(building, elevator);
               }
               break;
            case "exit":
               exitElevator(building, elevator);
               break;
            case "end":
               System.out.println("\n\nThanks for playing!!\n\n");
               System.exit(1);
               break;
            case "send":
               sendElevator(elevator);
               break;
            case "show":
               showPeopleOnFlors(building);
               break;
            case "elev":
               System.out.println("\n--------------------------------------");
               System.out.println("People in elevator: ");
               elevator.printPeople();
               System.out.println("--------------------------------------\n");
               break;
            case "check":
               if (winOrLoose(building, elevator)) {
                  System.out.println("\nCongrats you won\n");
                  System.exit(1);
               }
               else {
                  System.out.println("\nSorry you lost\n");
                  System.exit(1);
               }
               break;
            default:
               System.out.println("\nEnter one of the specified options\n");
               break;
         }
      }
      playAgain();

   }
   
   
   
   /**
    * The user gets to select from THREE difficulties, each difficulty in turn
    * determinate how hard the game will be to play and how much time you have
    * to complete the game
    * @return userInput
    */
   private  Level getLevelChoice() {

      String userInput = c.readLine("Please select a difficulty:\n"
                                    + "1.EASY\n"
                                    + "2.MEDIUM\n"
                                    + "3.HARD\n").toUpperCase();
      
      return Level.valueOf(userInput);
   }
   
   /**
    * Enters a Person into the elevator,
    * Also shows the user info about who is in the elevator, the user
    * has an option to load another person into the elevator
    * @param building
    * @param elevator 
    */
   public void enterElevator(Floor[] building, Elevator elevator) {

      System.out.println("\n--------------------------------------");
      Floor currentFloor = building[elevator.position()];
      showPeopleOnFlors(currentFloor);

      System.out.println();
      String name = c.readLine("Enter name of person to load: ");

      Person person = currentFloor.findPerson(name); 
      if (person != null) {

         currentFloor.exit(person);
         elevator.enter(person);
      }

      System.out.println("--------------------------------------\n");

   }
   /**
    * Exits a Person out off the elevator,
    * Also shows the user info about who is in the elevator, the user
    * has an option to exit another person out off the elevator
    * @param building
    * @param elevator 
    */
   private  void exitElevator(Floor[] building, Elevator elevator) {
      System.out.println("\n--------------------------------------");
      System.out.println("People in elevator: ");
      elevator.printPeople();

      Floor currentFloor = building[elevator.position()];

      System.out.println();
      String name = c.readLine("Enter name of person to unload: ");

      Person person = elevator.findPerson(name);
      elevator.exit(person);
      if (person != null) {
         currentFloor.addPerson(person);
      }

      System.out.println("--------------------------------------\n");

   }

   private  void sendElevator(Elevator elevator) {

      System.out.println("\n--------------------------------------");
      String stringIn = c.readLine("Enter floor 0 to 9:  ");

      int userInput = Integer.parseInt(stringIn);
      elevator.sendElevator(userInput);
      System.out.println("--------------------------------------\n");

   }

   private  void showPeopleOnFlors(Floor... building) {

      System.out.println("\n--------------------------------------");

      for (int i = 0; i < building.length; i++) {
         System.out.println("People on floor # " + i);
         building[i].printPeople();

      }

      System.out.println("--------------------------------------\n");

   }

   private  void playAgain() {
      String userInput = c.readLine("would you like to play again? y/n\n");
      switch (userInput) {
         case "y":
            getLevelChoice();
            break;
         case "n":
            System.exit(1);
            break;
         default:
            playAgain();
            break;
      }
   }

   private  boolean winOrLoose(Floor[] building, Elevator elevator) {
      boolean winCondition = true;
      if (!elevator.empty()) {
         winCondition = false;
      }
      else if (elevator.empty()) {

         for (int i = 0; i < building.length; i++) {
            List<Person> persons = building[i].getPersons();
            if (!persons.isEmpty()) {
               for (Person person : persons) {
                  if (person.getWantedFloor() != i) {
                     winCondition = false;
                  }
               }
            }
         }
      }

      return winCondition;
   }
   
   
   

}
