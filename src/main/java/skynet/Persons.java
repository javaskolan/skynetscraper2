package skynet;

/**
 * A utility class that populates floor
 *
 * @author codeCtupus
 */
public class Persons {

    /**
     *A list of names
     * 
     * to do: make file I/O
     */
    private static final String[] NAME_LIST
            = {"Olle", "Pelle", "Lisa", "Elke", "Juan", "Risto", "Peter",
                "Rick", "Eva", "Pekka", " Mike", "Nick", "Sten", "Ivar", "Tore"};

    private Persons() {
        // This is a utility class!
    }

    /**
     * Accepted range 1 to 15.
     *
     * @param floor
     * @param amountOfPersons
     */
    public static void populateFloor(Floor floor, int amountOfPersons) {

        if (amountOfPersons < 1 || amountOfPersons > 15) {
            throw new IllegalArgumentException("Blaaa!");
        }

        for (int i = 0; i < amountOfPersons; ++i) {
            Person p = new Person(NAME_LIST[i]);
            floor.addPerson(p);
        }

    }
}
