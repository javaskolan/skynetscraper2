package skynet;

import java.util.Objects;
import java.util.Random;

/**
 * handles the Person constructor as well as the methods returning values
 *
 * @author Swartt
 */
public class Person
{
    private final String name;
    private final int wantedFloor;
    

    Person(String name) {
        this.name = Objects.requireNonNull(name);
        this.wantedFloor = new Random().nextInt(9);
    }

    /**
     * Used to get the instanced based name of a person
     *
     * @return name
     */
    public String getName() {
        return this.name;
    }

    public int getWantedFloor() {
        return this.wantedFloor;
    }
}