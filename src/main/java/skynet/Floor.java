package skynet;

import java.util.ArrayList;
import java.util.List;

/**
 * Contains and initiates the multi-dimensional array {@code floors}
 *
 * @author c0deCtupus
 */
public class Floor {

    private final List<Person> persons = new ArrayList<>();

    public Floor() {

    }

    public void addPerson(Person p) {
        persons.add(p);
    }

    public void printPeople() {

        for (Person p : persons) {
            System.out.println(p.getName() + " Wants to  floor #  " + p.getWantedFloor());
        }

        System.out.print("\n\n");
    }

    public void exit(Person person) {
        persons.remove(person);

    }

    public boolean hasPerson(Person person) {
        return persons.contains(person);
    }

    public Person findPerson(String name) {
        for (Person p : persons) {
            if (p.getName().equals(name)) {
                return p;
            }
        }

        return null;
    }

    public List<Person> getPersons() {
        return persons;

    }

}
